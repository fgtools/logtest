file: README.txt - logtest project - 20130407

Just an app to write a log, and test logrotate in 
unix.

Have already tested using SPRTF(), and this went fine.

Now to test with ostream, ie SG_LOG type...

Maybe need to add a flush, like -
from : http://www.cplusplus.com/reference/ostream/ostream/flush/

// Flushing files
#include <fstream>      // std::ofstream

int main () {

  std::ofstream outfile ("test.txt");

  for (int n=0; n<100; ++n)
  {
    outfile << n;
    outfile.flush();
  }
  outfile.close();

  return 0;
}

But does syslog() support a FLUSH?

20130410: Successful compile and test in windows. Now to try Ubuntu...

All worked like a charm...

Had added a each 10 min cron job - to /etc/crontab
#m10 h d m w user   command
*/10 * * * * geoff  /home/geoff/bin/crontest2

Then in crontest2 put
#!/bin/sh
echo "10-min Test Successful: $(data)" >> /tmp/mintest.log
logrotate /home/geoff/projects/tests/logtest/build/logrotate

Then in logrotate put

C:\FG\18\logtest\build>type logrotate

/var/log/logtest/logtest.log {
 size 1k
 rotate 5
 compress
 copytruncate
 missingok
 notifempty
}

This application output a log message every 3 seconds to 
/var/log/logtest/logtest.log

AND THIS WAS SUCCESSFULLY ROTATED EVERY 10 MINUTES...

Had some doubt about the SG_LOG and SG_ALERT since they 
have not specific flush... so added a sequence number to each 
log output, to be able to check for any missing entry.

But remember reading somewhere that stream IO will 'flush' 
on receiving a newline character...

Anyway, after testing for MANY hours, it appeared not one 
single entry was missed... Even wrote a chklogseq.pl to 
read the set of LOGS and check the sequence was perfectly 
in order.

Now have no concerns about the fact that SG_LOG/SG_ALERT 
keep the file OPEN while the app is running, the logrotate 
command copytrucate worked like a charm.

Now push all this to https://gitorious.org/fgtools/logtest

Case closed...

# eof
