/*
 *  Crossfeed Client Project
 *
 *   Author: Geoff R. McLane <reports _at_ geoffair _dot_ info>
 *   License: GPL v2 (or later at your choice)
 *
 *   Revision 1.0.0  2012/10/17 00:00:00  geoff
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License as
 *   published by the Free Software Foundation; either version 2 of the
 *   License, or (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful, but
 *   WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, US
 *
 */

// Module: mpKeyboard.cxx
// Keyboard Polling
#include <stdio.h>
#ifdef _MSC_VER
#include <conio.h>
#else
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>  // unix file control
#include <termios.h>
#include <stdlib.h> // for system()
#endif
#include "mpKeyboard.hxx"

#ifdef _MSC_VER
int test_for_input(void)
{
    int chr = _kbhit();
    if (chr)
        chr = _getch();
    return chr;
}
#else /* !_MSC_VER */
// a unix port of int _kbhit()
// UGH - none of these worked ;=((
// still get an echo of input char like '^[' for ESC
// #define ADD_SHELL_OUT // needs more headers for system()
#define RETURN_CHAR
#define STDIN_NUM STDIN_FILENO
//#define STDIN_NUM fileno(stdin) // makes no difference!!!

int _kbhit(void)
{
  struct termios oldt, newt;
  int ch;
  int oldf;

  tcgetattr(STDIN_NUM, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_NUM, TCSANOW, &newt);
  oldf = fcntl(STDIN_NUM, F_GETFL, 0);
  fcntl(STDIN_NUM, F_SETFL, oldf | O_NONBLOCK);
#ifdef ADD_SHELL_OUT
  system("stty -echo"); // shell out to kill echo
  ch = getchar();
  system("stty echo");
#else  
  ch = getchar();
#endif

  tcsetattr(STDIN_NUM, TCSANOW, &oldt);
  fcntl(STDIN_NUM, F_SETFL, oldf);

#if (defined(RETURN_CHAR) || defined(ADD_SHELL_OUT))
  if (ch == EOF)
    ch = 0;
  return ch;
#else
  if(ch != EOF) {
    ungetc(ch, stdin);
    return 1;
  }

  return 0;
#endif
}
int test_for_input(void)
{
    int chr = _kbhit();
    
#if !(defined(RETURN_CHAR) || defined(ADD_SHELL_OUT))
    if (chr)
        chr = getchar();
#endif
    return chr;
}
#endif /* _MSC_VER y/n */

/* eof - mpKeyboard.cxx */
