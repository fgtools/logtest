#!/bin/sh

TMPSRC=".."
if [ ! -f "$TMPSRC/CMakeLists.txt" ]; then
    echo "ERROR: Can NOT find source $TMPSRC\CMakeLists.txt!"
    exit 1
fi

TMPOPTS=""

echo "Doing: 'cmake $TMPSRC $TMPOPTS'"
cmake $TMPSRC $TMPOPTS
if [ ! "$?" = "0" ]; then
    echo "ERROR: cmake config, generation FAILED!"
    exit 1
fi

echo "Doing: 'cmake --build . --config Release'"
cmake --build . --config Release
if [ ! "$?" = "0" ]; then
    echo "ERROR: cmake build error!"
    exit 1
fi

echo "Appears a success..."

# eof

