@setlocal
@set TMPPRJ=logtest
@set TMPLOG=bldlog-1.txt

@set TMPSRC=..
@if NOT EXIST %TMPSRC%\CMakeLists.txt goto ERR1

@set TMPOPTS=

@echo Build of %TMPPRJ% %DATE% %TIME% out to %TMPLOG%
@echo Build of %TMPPRJ% %DATE% %TIME% > %TMPLOG%

@echo Doing 'cmake %TMPSRC% %TMPOPTS%'
@echo Doing 'cmake %TMPSRC% %TMPOPTS%' >> %TMPLOG%
@cmake %TMPSRC% %TMPOPTS% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2

@echo Doing 'cmake --build . --config Debug'
@echo Doing 'cmake --build . --config Debug' >> %TMPLOG%
@cmake --build . --config Debug >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3

@echo Doing 'cmake --build . --config Release'
@echo Doing 'cmake --build . --config Release' >> %TMPLOG%
@cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR4

@echo Appears a successful build...

@goto END

:ERR1
@echo ERROR: Can NOT find source %TMPSRC%\CMakeLists.txt!
@goto ISERR

:ERR2
@echo ERROR: cmake config, genration FAILED!
@goto ISERR

:ERR3
@echo ERROR: cmake build Debug failed!
@goto ISERR

:ERR4
@echo ERROR: cmake build Release failed!
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof

