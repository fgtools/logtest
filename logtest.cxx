//
// logtest.cxx
// 20170619 - Rebuild with MSVC14 2015 1900
//
#include <sys/types.h>
#include <sys/stat.h>
#ifdef _MSC_VER
#pragma warning ( disable : 4996 )
//#include <Windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <unistd.h> // for sleep()
#include <string.h> // for strcmp()
#endif
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <iostream>
#include <fstream> // for ofstream
#include <ios> // for ios
#include <simgear/debug/logstream.hxx>

#include "logtest.hxx"
#include "mpKeyboard.hxx"
#include "sprtf.hxx"

using namespace std;

#ifndef LT_VERSION
#define LT_VERSION "0.0.99"
#endif


static const char *mod_name = "logtest";

static bool use_sg_log = true;

#ifdef _MSC_VER
#define sleep(a) Sleep(a * 1000)
const char *lt_log = "templt.txt";
#else // !_MSC_VER
const char *lt_log = "/var/log/logtest/logtest.log";
#endif // _MSC_VER y/n

static std::ofstream m_LogFile;
static std::string m_logName;


static time_t log_delay = 3;
static int do_alert = 0;
static int i_Sequence = 0;

int get_Sequence()
{
    i_Sequence++;
    return i_Sequence;
}

int Poll_Keyboard()
{
    int c = test_for_input();
    if ( c ) {
        int seq = get_Sequence();
        switch (c)
        {
        case 27:
            if (use_sg_log) {
                SG_ALERT(SG_IO, SG_DEBUG, seq << " Got EXIT key - ESC! " << c << " (0x" << std::hex << c << ")" );
            } else {
                SPRTF("%d Got EXIT key - ESC! %d (%x)\n", seq, c, c);
            }
            return 1;
            break;
        default:
            if (use_sg_log) {
                SG_ALERT(SG_IO, SG_DEBUG, seq << " Got unused key " << c << " (0x" << std::hex << c << ")" );
            } else {
                SPRTF("%d Unused key input! %d (%x)\n", seq, c, c);
            }
            break;
        }
    }
    return 0;
}

///////////////////////////////////////////////////
// establish output for the sglog()
void setLogFile( std::string & log )
{
    if (m_logName.size() && m_LogFile.is_open())
        m_LogFile.close();

    m_LogFile.open (log.c_str(), std::ios::out|std::ios::app);
    if (m_LogFile.fail() || !m_LogFile.is_open()) {
        fprintf(stderr,"%s: Open of log file %s FAILED!\n", mod_name, log.c_str());
        exit(1);
    }
    sglog().set_output (m_LogFile);
    m_logName = log;
}
///////////////////////////////////////////////////

void give_help()
{
    printf("%s [options]\n", mod_name);
    printf("Version %s, built %s, at %s\n", LT_VERSION, __DATE__, __TIME__);
    printf("options:\n");
    printf(" --help (-h or -?) = This help and exit 0\n");
    printf(" --log file   (-l) = Set log file. (def=%s)\n", lt_log );
    printf(" --sg on|off  (-s) = Use SG_LOG or SPRTF as logger method. (def=%s)\n", (use_sg_log ? "on" : "off"));
    printf(" --delay num  (-d) = Set seconds delay between log message. (def=%ld)\n", (long)log_delay);
    printf("Purpose: To output log messages regularly to test logrotate in unix.\n");
}

int is_on_off( char *arg )
{
    if (strcmp(arg,"on") == 0)
        return 1;
    if (strcmp(arg,"off") == 0)
        return 0;
    if (strcmp(arg,"1") == 0)
        return 1;
    if (strcmp(arg,"0") == 0)
        return 0;
    if (strcmp(arg,"yes") == 0)
        return 1;
    if (strcmp(arg,"no") == 0)
        return 0;
    fprintf(stderr,"Argument [%s] is NOT on, off, yes, no, 0, 1! Aborting...\n", arg);
    exit(1);
    return 2;
}


int parseCommands(int argc, char **argv)
{
    char *arg, *sarg;
    int i, i2;
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            if ((strcmp(sarg,"help") == 0) || (*sarg == 'h') || (*sarg == '?')) {
                give_help();
                exit(0);
            } else if (*sarg == 'l') {
                if (i2 < argc) {
                    sarg = argv[i2];
                    lt_log = strdup(sarg);
                    i++;
                    printf("%s: Set output log to [%s]\n", mod_name, lt_log);
                } else {
                    fprintf(stderr,"%s: ERROR: Log file name must follow.\n", mod_name);
                    goto Bad_Arg;
                }
            } else if (*sarg == 's') {
                if (i2 < argc) {
                    sarg = argv[i2];
                    use_sg_log = (is_on_off(sarg) ? true : false);
                    i++;
                    printf("%s: Set use SG_Log [%s]\n", mod_name, (use_sg_log ? "on" : "off"));
                } else {
                    fprintf(stderr,"%s: ERROR: on or off must follow.\n", mod_name);
                    goto Bad_Arg;
                }
            } else if (*sarg == 'd') {
                if (i2 < argc) {
                    sarg = argv[i2];
                    log_delay = atoi(sarg);
                    i++;
                    printf("%s: Set log delay to %ld seconds.\n", mod_name, (long)log_delay);
                } else {
                    fprintf(stderr,"%s: ERROR: on or off must follow.\n", mod_name);
                    goto Bad_Arg;
                }
            }
        } else {
Bad_Arg:
            fprintf(stderr,"%s: ERROR: Bad or unknown argument [%s]\n", mod_name, arg );
            return 1;
        }
    }
    return 0;
}


int main( int argc, char **argv )
{
    int seq;
    if (parseCommands(argc,argv))
        return 1;

    std::string log(lt_log);
    printf("%s: Using LOG file [%s]. Logging each %ld secs.\n", mod_name, log.c_str(), (long)log_delay);
    
    seq = get_Sequence();
    if (use_sg_log) {
        sglog().setLogLevels( SG_ALL, SG_DEBUG );
        sglog().enable_with_date (true);
        setLogFile( log );
        SG_ALERT( SG_SYSTEMS, SG_ALERT, seq << " Running " << argv[0] << ", entering loop... ESC to exit." );
    } else {
        add_append_log(1);
        add_sys_date(1);
        set_log_file((char *)log.c_str());
        SPRTF("%d Running %s, entering loop... ESC to exit\n", seq, argv[0]);
    }


    time_t last, now, log_last;

    last = 0;
    log_last = 0;
    while (1) {
        now = time(0);
        if (now != last) {
            if (Poll_Keyboard())
                break;
            last = now;
        }
        sleep(1);
        if (now >= (log_last + log_delay)) {
            seq = get_Sequence();
            if (use_sg_log) {
                do_alert++;
                if (do_alert >= 5) {
                    do_alert = 0;
                    SG_ALERT( SG_SYSTEMS, SG_ALERT, seq << " Alert log message..." );
                } else {
                    SG_LOG (SG_IO, SG_DEBUG, seq << " Next log message..." );
                }

            } else {
                SPRTF("%d Next log message.\n", seq);
            }
            log_last = now;

        }
    }
    
  
    seq = get_Sequence();
    if (use_sg_log) {
        SG_ALERT( SG_SYSTEMS, SG_ALERT, seq << " Ending " << argv[0] );
    } else {
        SPRTF("%d Ending %s\n", seq, argv[0]);
    }
    return 0;
}

// eof - logtest.cxx

